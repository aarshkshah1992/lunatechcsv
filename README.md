**Running the tests**  
   
$> cd lunatechcsv  
$> sbt test   

**Service Endpoints**      

By default, the service starts on port 8080. It can be changed by changing the port  
number in the application.conf file at src/main/resources/application.conf. The service keeps on running    
till any key is pressed at the console   

All requests are HTTP GET requests and responses are in Json.      
   
**Query by Country Name**:     
GET "/query/countryName/{name of country}"   

**Query by Country Code**:     
GET "/query/countryCode/{country code}"       

**Generate Report**:     
GET "/report"       

**Note**    
Both the bonus challenges have been implemented

