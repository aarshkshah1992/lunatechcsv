package csvserving.model

import csvserving.{AirportId, CountryCode, CountryName, RunwayId}

case class QueryResponse(countryName: CountryName, countryCode: CountryCode,
                         airports: Set[Airport], runways: Set[Runway])

case class Airport(id: AirportId, name: String)

case class Runway(id: RunwayId, surface: String)