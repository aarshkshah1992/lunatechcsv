package csvserving.model

import csvserving.{CountryCode, CountryName}

sealed trait ErrorResponse {
  def status: Int

  def errorMsg: String
}

case class InValidCountryCode(code: CountryCode) extends ErrorResponse {
  override def errorMsg: String = s"Invalid country code $code"

  override def status: Int = 400
}

case class InvalidCountryName(name: CountryName) extends ErrorResponse {
  override def errorMsg: String = s"Invalid country name $name"

  override def status: Int = 400
}

case class ErrorResponseMsg(erroMsg: String)