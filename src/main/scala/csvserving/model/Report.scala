package csvserving.model

import csvserving.{CountryName, RunwayIdentifier, RunwayType}

case class CountryAirportFrequency(countryName: CountryName, nAirports: Int)

case class Report(countriesWithHighestAirports: List[CountryAirportFrequency],
                  countriesWithLowestAirports: List[CountryAirportFrequency],
                  runwayType: Map[CountryName, Set[RunwayType]],
                  commonRunwayIdentifiers: List[RunwayIdentifier])