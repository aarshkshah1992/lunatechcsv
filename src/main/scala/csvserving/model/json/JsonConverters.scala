package csvserving.model.json

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import csvserving.model._
import spray.json.DefaultJsonProtocol

trait JsonConverters extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val airportFmt = jsonFormat2(Airport)
  implicit val runwayFmt = jsonFormat2(Runway)
  implicit val queryResponseFmt = jsonFormat4(QueryResponse)
  implicit val errorRespFmt = jsonFormat1(ErrorResponseMsg)
  implicit val countryFreqFmt = jsonFormat2(CountryAirportFrequency)
  implicit val reportFmt = jsonFormat4(Report)
}