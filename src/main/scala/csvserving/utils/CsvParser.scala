package csvserving.utils

import java.io.File

import com.github.tototoshi.csv.CSVReader

object CsvParser {
  def parseAndGetMappings(resourcePath: String) = {
    val filePath = getClass.getResource(resourcePath).getPath
    val reader = CSVReader.open(new File(filePath))
    reader.allWithHeaders()
  }
}