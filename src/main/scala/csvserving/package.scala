import com.typesafe.config.ConfigFactory

package object csvserving {
  val conf = ConfigFactory.load()
  type CountryCode = String
  type CountryName = String
  type AirportId = String
  type RunwayId = String
  type RunwayType = String
  type RunwayIdentifier = String
}