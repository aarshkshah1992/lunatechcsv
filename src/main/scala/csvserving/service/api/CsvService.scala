package csvserving.service.api

import csvserving.model.{ErrorResponse, QueryResponse, Report}
import csvserving.{CountryCode, CountryName}

trait CsvService {
  def getQueryResponseForCountryName(countryName: CountryName): Either[ErrorResponse, QueryResponse]

  def getQueryResponseForCountryCode(countryCode: CountryCode): Either[ErrorResponse, QueryResponse]

  def getReport: Report
}
