package csvserving.service.impl

import csvserving.model._
import csvserving.service.api.CsvService
import csvserving._
import csvserving.utils.CsvParser

class CsvServiceImpl extends CsvService {
  override def getQueryResponseForCountryName(countryName: CountryName): Either[ErrorResponse, QueryResponse] =
    for {
      countryCode <- getCountryCode(countryName).map(Right(_)).getOrElse(Left(InvalidCountryName(countryName)))
      resp <- getQueryResponseForCountryCode(countryCode)
    } yield resp


  override def getQueryResponseForCountryCode(countryCode: CountryCode): Either[ErrorResponse, QueryResponse] = {
    val normalizedCountryCode = countryCode.toUpperCase
    for {
      airports <- countryCodeToAirports.get(normalizedCountryCode).map(Right(_)).getOrElse(Left(InValidCountryCode(countryCode)))
      runways = airports.flatMap(airport => airportIdToRunways.get(airport.id)).flatten
    } yield QueryResponse(countryCodeToCountryName(normalizedCountryCode), normalizedCountryCode, airports, runways)
  }

  override def getReport: Report = report


  private def getCountryCode(partiaOrFullName: String): Option[CountryCode] =
    countryNameToCountryCode.get(partiaOrFullName.toUpperCase).orElse {
      countryNameToCountryCode.keySet.find {
        _.startsWith(partiaOrFullName.toUpperCase)
      }.flatMap(countryNameToCountryCode.get(_))
    }


  private val airPortsFilePath =
    conf.getString("file-paths.airports")
  private val runwaysFilePath =
    conf.getString("file-paths.runways")
  private val countriesFilePath =
    conf.getString("file-paths.countries")

  private val airportMappings =
    CsvParser.parseAndGetMappings(airPortsFilePath)

  private val runwayMappings =
    CsvParser.parseAndGetMappings(runwaysFilePath)

  private val countryMappings =
    CsvParser.parseAndGetMappings(countriesFilePath)

  private lazy val countryNameToCountryCode: Map[CountryName, CountryCode] =
    countryMappings.map(mapping => (mapping("name").toUpperCase, mapping("code").toUpperCase)).toMap

  private lazy val countryCodeToCountryName: Map[CountryCode, CountryName] =
    countryNameToCountryCode.map {
      case (name, code) => (code, name)
    }

  private lazy val countryCodeToAirports: Map[CountryCode, Set[Airport]] =
    airportMappings.groupBy(mapping => mapping("iso_country")).mapValues {
      airports => airports.map(airport => Airport(airport("id"), airport("name"))).toSet
    }

  private lazy val airportIdToRunways: Map[AirportId, Set[Runway]] =
    runwayMappings.groupBy(mapping => mapping("airport_ref")).mapValues {
      runways => runways.map(runway => Runway(runway("id"), runway("surface"))).toSet
    }

  private lazy val commonRunwayIdentifiers =
    runwayMappings.groupBy(mapping => mapping("le_ident")).filterNot(_._1.isEmpty).toList
      .sortBy {
        case (identifier, instances) => instances.size
      }.takeRight(10).map(_._1)


  private lazy val report = {
    val sortedByAirporFreq = countryCodeToAirports.mapValues(_.size).toList.sortBy(_._2).map {
      case (countryCode, nAirports) => CountryAirportFrequency(countryCodeToCountryName(countryCode), nAirports)
    }

    val highestAirports = sortedByAirporFreq.takeRight(10)

    val lowestAirports = sortedByAirporFreq.take(10)

    val countryToRunwayTypes = countryCodeToAirports.map {
      case (countryCode, airports) =>
        (countryCodeToCountryName(countryCode), airports.flatMap {
          airport => airportIdToRunways.get(airport.id).map(runways => runways.map(_.surface.toUpperCase).filterNot(_.isEmpty))
        }.flatten)
    }

    Report(highestAirports, lowestAirports, countryToRunwayTypes, commonRunwayIdentifiers)
  }
}