package csvserving.service

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.PathMatchers.Segment
import akka.stream.ActorMaterializer
import csvserving.model.{ErrorResponse, ErrorResponseMsg}
import csvserving.model.json.JsonConverters
import csvserving.service.api.CsvService
import spray.json.RootJsonFormat

trait Service {
  self: JsonConverters =>

  implicit def system: ActorSystem

  implicit def materializer: ActorMaterializer

  def csvService: CsvService

  val routes =
    get {
      pathPrefix("query") {
        path("countryName" / Segment) {
          countryName =>
            val resp = csvService.getQueryResponseForCountryName(countryName)
            sendResponse(resp)
        } ~ path("countryCode" / Segment) {
          countryCode =>
            val resp = csvService.getQueryResponseForCountryCode(countryCode)
            sendResponse(resp)
        }
      } ~ path("report") {
        val resp = csvService.getReport
        complete(resp)
      }
    }


  private def sendResponse[T: RootJsonFormat](resp: Either[ErrorResponse, T]) = {
    resp.fold(l => complete(l.status, ErrorResponseMsg(l.errorMsg)), r => complete(r))
  }
}