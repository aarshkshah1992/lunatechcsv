package csvserving.service

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import csvserving.model.json.JsonConverters
import csvserving.service.impl.CsvServiceImpl
import csvserving._

import scala.io.StdIn

object ServerMain extends App with Service with JsonConverters {
  override implicit val system = ActorSystem()
  override implicit val materializer = ActorMaterializer()
  override lazy val csvService = new CsvServiceImpl

  implicit val executionContext = system.dispatcher

  val port = conf.getInt("service.port")
  val bindingFuture = Http().bindAndHandle(routes, "localhost", port)
  println(s"Server online at http://localhost:$port/\nPress RETURN to stop...")

  StdIn.readLine()

  bindingFuture
    .flatMap(_.unbind())
    .onComplete(_ ⇒ system.terminate())
}