package csvserving.service

import csvserving.BaseSuite
import csvserving.utils.Helpers._

class ReportTests extends BaseSuite {
  val sut = csvService

  test("report contents should be semantically correct") {
    val report = sut.getReport

    report.commonRunwayIdentifiers.size shouldBe 10
    report.runwayType should not be empty

    report.countriesWithHighestAirports.size shouldBe 10
    report.countriesWithHighestAirports.last.countryName shouldBe "UNITED STATES"

    report.countriesWithLowestAirports.size shouldBe 10
  }

}