package csvserving.service

import csvserving.BaseSuite
import csvserving.model._
import csvserving.service.impl.CsvServiceImpl
import csvserving.utils.Helpers._

class QueryTests extends BaseSuite {
  val sut = csvService

  test("invalid country name returns error") {
    sut.getQueryResponseForCountryName("random").left.value shouldBe InvalidCountryName("random")
  }

  test("invalid country code returns error") {
    sut.getQueryResponseForCountryCode("random").left.value shouldBe InValidCountryCode("random")
  }

  test("expected response for valid country name") {
    sut.getQueryResponseForCountryName("MicroNeSIA").right.value shouldBe microNesiaQueryResponse
  }

  test("expected response for valid country code") {
    sut.getQueryResponseForCountryCode("fM").right.value shouldBe microNesiaQueryResponse
  }

  test("fuzzy country name logic works and correct query response is returned") {
    sut.getQueryResponseForCountryName("MicrOn").right.value shouldBe microNesiaQueryResponse
  }
}