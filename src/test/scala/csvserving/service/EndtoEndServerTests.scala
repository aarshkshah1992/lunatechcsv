package csvserving.service

import akka.http.scaladsl.testkit.ScalatestRouteTest
import csvserving.BaseSuite
import csvserving.model.json.JsonConverters
import csvserving.service.impl.CsvServiceImpl
import akka.http.scaladsl.model.StatusCodes._
import csvserving.model.{ErrorResponseMsg, QueryResponse, Report}
import csvserving.utils.Helpers
import csvserving.utils.Helpers._

class EndtoEndServerTests extends BaseSuite with ScalatestRouteTest with Service with JsonConverters {
  override val csvService = Helpers.csvService

  test("invalid country code should return 400") {
    Get("/query/countryCode/random") ~> routes ~> check {
      status shouldBe BadRequest
      responseAs[ErrorResponseMsg].erroMsg should not be empty
    }
  }

  test("invalid country name should return 400") {
    Get("/query/countryName/random") ~> routes ~> check {
      status shouldBe BadRequest
      responseAs[ErrorResponseMsg].erroMsg should not be empty
    }
  }

  test("expected query response for Micronesia country name") {
    Get("/query/countryName/micronesIa") ~> routes ~> check {
      status shouldBe OK
      responseAs[QueryResponse] shouldBe microNesiaQueryResponse
    }
  }

  test("expected query response for FM country code") {
    Get("/query/countryCode/Fm") ~> routes ~> check {
      status shouldBe OK
      responseAs[QueryResponse] shouldBe microNesiaQueryResponse
    }
  }

  test("expected response for report request") {
    Get("/report") ~> routes ~> check {
      status shouldBe OK
      responseAs[Report].countriesWithLowestAirports should not be empty
    }
  }

}