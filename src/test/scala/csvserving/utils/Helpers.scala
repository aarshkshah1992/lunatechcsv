package csvserving.utils

import csvserving.model.{Airport, QueryResponse, Runway}
import csvserving.service.impl.CsvServiceImpl

object Helpers {
  val microNesiaQueryResponse = QueryResponse("MICRONESIA", "FM", Set(Airport("32538", "Falalop Island Airport"), Airport("5499", "Chuuk International Airport"), Airport("44078", "Woleai Atoll Airport"), Airport("5503", "Yap International Airport"), Airport("5502", "Kosrae International Airport"), Airport("5500", "Pohnpei International Airport")), Set(Runway("235787", "ASP"), Runway("235785", "ASP"), Runway("235784", "ASP"), Runway("235786", "ASP")))
  val csvService = new CsvServiceImpl
}
